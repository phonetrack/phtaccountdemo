window.App = window.App || {};

App.AdminCreateUserComponent = React.createClass({
    getInitialState: function() {
        return {
            "account-name": "",
            "account-realm": "",
            "user-firstname": "",
            "user-lastname": "",
            "user-email": "",
            "user-password": "",
            "device-name": "",
            "token": ""
        };
    },

    render: function() {
        return(
            <div className="admin-user-create-component">
                <div className="row">
                    <div className="col-md-8">
                        <form onSubmit={this.onSubmit}>
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3>Novo usuário</h3>
                                </div>

                                <div className="panel-body">
                                    <div className="form-group" ref={this.refElement} id="account-name-group">
                                        <label className="control-label">Nome da conta</label>
                                        <input
                                            value={this.state["account-name"]}
                                            className="form-control" />
                                    </div>

                                    <div className="form-group" ref={this.refElement} id="account-realm-group">
                                        <label className="control-label">Realm</label>
                                        <input
                                            value={this.state["account-realm"]}
                                            className="form-control" />
                                    </div>

                                    <div className="form-group" ref={this.refElement} id="user-firstname-group">
                                        <label className="control-label">Nome do usuário</label>
                                        <input
                                            value={this.state["user-firstname"]}
                                            className="form-control" />
                                    </div>

                                    <div className="form-group" ref={this.refElement} id="user-lastname-group">
                                        <label className="control-label">Sobrenome do usuário</label>
                                        <input
                                            value={this.state["user-lastname"]}
                                            className="form-control" />
                                    </div>

                                    <div className="form-group" ref={this.refElement} id="user-email-group">
                                        <label className="control-label">Email</label>
                                        <input
                                            value={this.state["user-email"]}
                                            className="form-control" />
                                    </div>

                                    <div className="form-group" ref={this.refElement} id="user-password-group">
                                        <label className="control-label">Email</label>
                                        <input
                                            value={this.state["user-password"]}
                                            className="form-control" />
                                    </div>

                                    <div className="form-group" ref={this.refElement} id="device-name-group">
                                        <label className="control-label">Nome do acesso</label>
                                        <input
                                            value={this.state["device-name"]}
                                            className="form-control" />
                                    </div>
                                </div>
                                <div className="panel-footer">
                                    <button className="btn btn-default">Cancelar</button>&nbsp;
                                    <button className="btn btn-primary">Criar usuário</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="col-md-4">
                    </div>
                </div>
            </div>
        );
    },

    onSubmit: function(event) {
        event.preventDefault();
        this.create();
    },

    refElement: function(element) {

    },

    create: function() {

    },

    validate: function() {

    }
});
