window.App = window.App || {};

App.AdminUserComponent = React.createClass({
    render: function() {
        return(
            <div>
                {this.props.children}
            </div>
        );
    }
});
