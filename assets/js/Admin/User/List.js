window.App = window.App || {};

App.AdminUserListComponent = React.createClass({
    getInitialState: function() {
        return {
            users: []
        }
    },

    componentWillMount: function() {
        this.updateList();
    },

    render: function() {
        var rows;

        rows = this.state.users.map(function(user) {
            return (
                <tr>
                    <td>{user.username}</td>
                    <td>{user.first_name}</td>
                    <td>{user.last_name}</td>
                    <td>
                        <a className="btn btn-default btn-xs" href="#">Alterar senha</a> &nbsp;
                        <a className="btn btn-default btn-xs" href="#">Remover</a>
                    </td>
                </tr>
            );
        });

        return(
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3>Listagem de usuários</h3>
                </div>
                <div className="panel-body">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Nome</th>
                                <th>Sobrenome</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>{rows}</tbody>
                    </table>
                </div>
            </div>
        );
    },

    updateList: function() {
        $.ajax({
            url: "http://localhost:3002/users",
            type: "get",
            dataType: "json",
            data: {
                account_id: localStorage.getItem("account_id"),
                token: localStorage.getItem("token")
            },
            success: this.onUpdateListSuccess
        });
    },

    onUpdateListSuccess: function(data) {
        if(data.status == "success") {
            this.state.users = data.data
            this.setState(this.state);
        }
    }
});
