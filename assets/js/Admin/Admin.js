window.App = window.App || {};

App.AdminComponent = React.createClass({
    componentWillMount: function() {
        if(!localStorage.getItem("token")) {
            ReactRouter.hashHistory.push("/login");
        }
    },

    render: function() {
        if(!localStorage.getItem("token")) {
            ReactRouter.hashHistory.push("/login");
        }

        return(
            <div>
                <nav className="navbar navbar-default navbar-fixed-top">
                    <div className="container">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <ReactRouter.Link className="navbar-brand" to="/admin">HypnoPhone</ReactRouter.Link>
                                {this.renderMenu()}
                            </div>
                        </div>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    renderMenu: function() {
        return;
        return (
            <ul className="nav navbar-nav">
                <li><ReactRouter.Link to="/admin">Home</ReactRouter.Link></li>
                <li className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuários <span className="caret"></span></a>
                    <ul className="dropdown-menu">
                        <li><ReactRouter.Link to="/admin/users">Ver usuários</ReactRouter.Link></li>
                        <li><ReactRouter.Link to="/admin/users/create">Novo usuário</ReactRouter.Link></li>
                    </ul>
                </li>
            </ul>
        );
    }
});
