window.App = window.App || {};

App.AdminTransactionsDetails = React.createClass({
    getRows: function() {
        return this.props.data.map(function(item) {
            var date, from, to;

            if(item.created) {
                date = new Date((item.created - 62167219200) * 1000);
                from = item.metadata.from.split("@");
                to = item.metadata.to.split("@");

                return (
                    <tr className={item.type}>
                        <td><span className={"glyphicon glyphicon-arrow-" + (item.type == 'credit' ? "right" : "left")} ariaHidden="true"></span></td>
                        <td>{date.getDate()}/{date.getMonth()}/{date.getFullYear()} {date.getHours()}:{date.getMinutes()}:{date.getSeconds()}</td>
                        <td>{from[0]}</td>
                        <td>{to[0]}</td>
                        <td>{item.sub_account_name}</td>
                        <td>{Math.ceil(item.metadata.duration / 60)}</td>
                        <td>R$ {item.amount.toFixed(2).replace(".", ",")}</td>
                    </tr>
                );
            }
        });
    },

    render: function() {
        return(
            <div className="admin-transactions-details-component">
                <div className="details-table">
                    <table className="table table-stripped table-hover">
                        <thead>
                            <th></th>
                            <th>Data</th>
                            <th>De</th>
                            <th>Para</th>
                            <th>Conta</th>
                            <th>Minutos</th>
                            <th>Crédito consumido</th>
                        </thead>
                        <tbody>
                            {this.getRows()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
});
