window.App = window.App || {};

App.AdminTransactionsFilters = React.createClass({
    getInitialState: function() {
        return {
            filters: {
                created_from: null,
                created_to: null
            }
        };
    },

    componentDidMount: function() {
        $(".admin-transactions-filters-component .form-control")
            .datepicker({
                format: "dd/mm/yyyy",
                language: "pt-BR",
                minViewMode: 1,
                format: {
                    toDisplay: function(date, format, language) {
                        return $.fn.datepicker.dates['pt-BR'].months[date.getMonth() + 1];
                    },
                    toValue: function(date) {
                        return date.getTime() / 1000;
                    }
                }
            })
            .on("changeDate", this.onChangeHandler);
    },

    render: function() {
        return(
            <div className="admin-transactions-filters-component">
                <div className="row">
                    <div className="col-md-2">
                        <div className="input-group">
                            <input type="text" className="form-control" placeholder="Competência" onChange={this.onChangeHandler} id="created_from" />
                            <span className="input-group-btn">
                                <button className="btn btn-primary">
                                    <span className="glyphicon glyphicon-calendar" ariaHidden="true"></span>
                                </button>
                            </span>
                        </div>
                    </div>

                    <div className="col-md-3">
                        <button className="btn btn-warning" onClick={this.onFilterClick}>Filtrar</button> &nbsp;
                        <button className="btn btn-success" onClick={this.onDownloadClick} disabled={!this.props.hasData}>Download</button>
                    </div>
                </div>
            </div>
        );
    },

    onChangeHandler: function(event) {
        var created_from = $(event.target).datepicker("getDate");
        var created_to = new Date(created_from);

        created_to.setMonth(created_to.getMonth() + 1);
        created_to.setDate(created_to.getDate() - 1);
        created_to.setHours(23);
        created_to.setMinutes(59);
        created_to.setSeconds(59);

        this.state.filters.created_from = (created_from.getTime() / 1000) + 62167219200;
        this.state.filters.created_to = (created_to.getTime() / 1000) + 62167219200;
        this.setState(this.state);
    },

    onFilterClick: function(event) {
        event.preventDefault();
        this.props.update(this.state.filters);
    },

    onDownloadClick: function(event) {
        event.preventDefault();
        this.props.download();
    }
    
});
