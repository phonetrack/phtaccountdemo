window.App = window.App || {};

App.AdminTransactionsWidget = React.createClass({
    render: function() {
        return(
            <div className="admin-transactions-widget-component">
                <div className="row">
                    <div className="col-md-2">
                        <div className="widget-block">
                            <h3 className="widget-value">R$ {this.props.amount.toFixed(2).replace(".", ",")}</h3>
                            <h5 className="widget-label">Saldo disponível</h5>
                        </div>
                    </div>

                    <div className="col-md-2 col-md-offset-6">
                        <div className="widget-block">
                            <h3 className="widget-value">{this.props.minutesUsed}</h3>
                            <h5 className="widget-label">Minutos utilizados</h5>
                        </div>
                    </div>

                    <div className="col-md-2">
                        <div className="widget-block">
                            <h3 className="widget-value">R$ {this.props.callCharges.toFixed(2).replace(".", ",")}</h3>
                            <h5 className="widget-label">Crédito consumido</h5>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});
