window.App = window.App || {};

App.AdminTransactionsIndex = React.createClass({
    getInitialState: function() {
        return {amount: 0, callCharges: 0, minutesUsed: 0, data: []}
    },

    componentWillMount: function() {
        this.update();
        this.getCurrentBalance();
    },

    render: function() {
        return (
            <div clsasName="admin-transactions-index-component">
                <div className="row">
                    <div className="col-md-12">
                        <App.AdminTransactionsWidget amount={this.state.amount} callCharges={this.state.callCharges} minutesUsed={this.state.minutesUsed}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <App.AdminTransactionsFilters update={this.update} download={this.download} hasData={this.state.data.length > 0}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <App.AdminTransactionsDetails data={this.state.data} update={this.update}/>
                    </div>
                </div>
            </div>
        );
    },

    download: function() {
        var str = '';

        for (var i = 0; i < this.state.data.length; i++) {
            var line = '';
            for (var index in this.state.data[i]) {
                if (line != '')
                    line += ','

                line += this.state.data[i][index];
            }

            str += line + '\r\n';
        }

        var uri = 'data:text/csv;charset=utf-8,' + escape(str);
        var downloadLink = document.createElement("a");
        downloadLink.href = uri;
        downloadLink.download = "data.csv";

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    },

    update: function(filters) {
        filters = filters || {};
        filters.reason = "only_calls";

        axios({
            url: App.config.apiEndpointPrefix + "/accounts/" + localStorage.getItem("account_id") + "/transactions",
            method: 'get',
            params: filters,
            headers: {
                "X-Auth-Token": localStorage.getItem("token")
            }
        }).then(this.onUpdateSuccess).catch(this.onUpdateError);
    },

    getCurrentBalance: function() {
        axios({
            url: App.config.apiEndpointPrefix + "/accounts/" + localStorage.getItem("account_id") + "/transactions/current_balance",
            method: 'get',
            headers: {
                "X-Auth-Token": localStorage.getItem("token")
            }
        }).then(this.onGetCurrentBalanceSuccess);
    },

    onGetCurrentBalanceSuccess: function(response) {
        if (response.data.status == "error") {
            if (response.data.error == 401) {
                ReactRouter.hashHistory.push("/login");
            }
            return;
        }

        this.state.amount = response.data.data.balance;
        this.setState(this.state);
    },

    onUpdateSuccess: function(response) {
        var dataObj,
            item;

        dataObj = {};

        if (response.data.data.status == "error") {
            if (data.error == 401) {
                ReactRouter.hashHistory.push("/login");
            }

            return;
        }

        this.state.callCharges = 0;
        this.state.minutesUsed = 0;
        this.state.data = response.data.data;

        for (var i in response.data.data) {
            item = response.data.data[i];

            if (item.type == "debit") {
                if (item.amount !== undefined) {
                    this.state.callCharges += item.amount;
                }

                if (item.metadata.duration !== undefined) {
                    this.state.minutesUsed += Math.ceil(item.metadata.duration / 60);
                }
            }

        }

        this.setState(this.state);
    }

});
