window.App = window.App || {};

App.LoginComponent = React.createClass({
    componentWillMount: function() {
        this.elements = {};
        this.setState({
            username: "",
            password: "",
            account_name: ""
        });
    },

    submitHandler: function(event) {
        event.preventDefault();
        if(this.validate()) {
            this.auth();
        }

    },

    onChangeHandler: function(event) {
        this.state[event.target.name] = event.target.value;
        this.setState(this.state);
    },

    onBlurHandler: function(event) {
        if(this.state[event.target.name]) {
            this.elements[event.target.name + "-group"].classList.remove("has-error");
        } else {
            this.elements[event.target.name + "-group"].classList.add("has-error");
        }
    },

    render: function() {
        return (
            <div className="login-component">
                <div className="login-component-container">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 col-md-offset-3">
                                <div className="panel panel-default">
                                    <div className="panel-body">
                                        <h3>Login</h3>

                                        <form onSubmit={this.submitHandler}>
                                            <div id="username-group" className="form-group form-group-user" ref={this.refElement}>
                                                <label className="control-label">Usuário</label>
                                                <input
                                                    onBlur={this.onBlurHandler}
                                                    onChange={this.onChangeHandler}
                                                    name="username"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.username} />
                                            </div>

                                            <div id="password-group" className="form-group form-group-password" ref={this.refElement}>
                                                <label className="control-label">Senha</label>
                                                <input
                                                    onBlur={this.onBlurHandler}
                                                    onChange={this.onChangeHandler}
                                                    name="password"
                                                    type="password"
                                                    className="form-control"
                                                    value={this.state.password} />
                                            </div>

                                            <div id="account_name-group" className="form-group form-group-account" ref={this.refElement}>
                                                <label className="control-label">Conta</label>
                                                <input
                                                    onBlur={this.onBlurHandler}
                                                    onChange={this.onChangeHandler}
                                                    name="account_name"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.account_name} />
                                            </div>

                                            <div className={this.state.error ? 'alert alert-danger' : 'hidden'}>{this.state.error}</div>

                                            <button className="btn btn-primary">Login</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    refElement: function(element) {
        if(element) {
            this.elements[element.id] = element;
        }
    },

    validate() {
        var error = false;

        if(!this.state.username) {
            this.elements["username-group"].classList.add("has-error");
            error = true;
        }

        if(!this.state.password) {
            this.elements["password-group"].classList.add("has-error");
            error = true;
        }

        if(!this.state.account_name) {
            this.elements["account_name-group"].classList.add("has-error");
            error = true;
        }

        return !error;
    },

    auth: function() {
        axios({
            url: App.config.apiEndpointPrefix + "/user_auth",
            method: 'put',
            data: Qs.stringify({
                username: this.state.username,
                password: this.state.password,
                account_name: this.state.account_name
            }),
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
            .then(this.onAuthSuccess)
            .catch(this.onAuthError)
    },

    onAuthSuccess: function(response) {
        if(response.data.status == "error") {
            switch (response.data.error) {
                case "401":
                case "400":
                    this.state.error = "Credenciais inválidas. Por favor, verifique e tente novamente.";
                    this.setState(this.state);
                    break;
                default:
            }

            return;
        }

        localStorage.setItem("token", response.data.auth_token);
        localStorage.setItem("account_id", response.data.data.account_id);
        ReactRouter.hashHistory.push("/admin");
    },

    onAuthError: function(error) {
        this.state.error = "Credenciais inválidas. Por favor, verifique e tente novamente.";
        this.setState(this.state);
    }
});
