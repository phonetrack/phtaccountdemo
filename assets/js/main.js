window.App = window.App || {};

App.App = React.createClass({
    render: function() {
        return(
            <div>
                {this.props.children}
            </div>
        );
    }
});

ReactDOM.render(
    <ReactRouter.Router history={ReactRouter.hashHistory}>
        <ReactRouter.Route path="/" component={App.App}>
            <ReactRouter.IndexRoute component={App.LoginComponent}/>
            <ReactRouter.Route path="login" component={App.LoginComponent} />

            <ReactRouter.Route path="admin" component={App.AdminComponent}>
                <ReactRouter.IndexRoute component={App.AdminTransactionsIndex} />

                <ReactRouter.Route path="transactions" component={App.AdminTransactionsIndex} />
                <ReactRouter.Route path="users" component={App.AdminUserComponent}>
                    <ReactRouter.IndexRoute component={App.AdminUserListComponent} />
                    <ReactRouter.Route path="create" component={App.AdminCreateUserComponent} />
                </ReactRouter.Route>
            </ReactRouter.Route>
        </ReactRouter.Route>
    </ReactRouter.Router>,
    document.getElementById('app')
);
