import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import "bootstrap-datepicker/dist/css/bootstrap-datepicker.css";
import "./main.css";

import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min';
import 'bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min';

import React from "react";
import { render } from "react-dom";
import { Router, Route, Link, IndexRoute, hashHistory } from 'react-router';

import { AppComponent } from "./App";
import { LoginComponent, AutoLoginComponent } from "./Login";
import { AdminComponent } from "./Admin/Admin";
import { AdminTransactionsIndexComponent } from "./Admin/Transactions/Index";

render((
    <Router history={hashHistory}>
        <Route path="/" component={AppComponent}>
            <IndexRoute component={LoginComponent}/>
            <Route path="login" component={LoginComponent} />
            <Route path="login/:account_name/:credentials" component={AutoLoginComponent} />

            <Route path="admin" component={AdminComponent}>
                <IndexRoute component={AdminTransactionsIndexComponent} />
                <Route path="transactions" component={AdminTransactionsIndexComponent} />
            </Route>
        </Route>
    </Router>
), document.getElementById('app'));
