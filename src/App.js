import React, { Component } from "react";

export class AppComponent extends Component {
    render() {
        return(
            <div>
                {this.props.children}
            </div>
        );
    }
}
