import React, { Component } from "react";
import { hashHistory } from 'react-router';
import { stringify } from "qs";
import md5 from "md5";
import { auth } from "./Service/auth";
import loading from "./loader.gif";

export class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.elements = {};
        this.state = {
            username: "",
            password: "",
            account_name: ""
        };

    }

    componentWillMount() {
        $("body").addClass("page-login");
    }

    componentWillUnmount() {
        $("body").removeClass("page-login");
    }

    submitHandler(event) {
        event.preventDefault();
        if(this.validate()) {
            auth(md5(`${this.state.username}:${this.state.password}`), this.state.account_name)
                .then(this.onAuthSuccess.bind(this))
                .catch(this.onAuthError.bind(this));
        }

    }

    onChangeHandler(event) {
        this.state[event.target.name] = event.target.value;
        this.setState(this.state);
    }

    onBlurHandler(event) {
        if(this.state[event.target.name]) {
            this.elements[event.target.name + "-group"].classList.remove("has-error");
        } else {
            this.elements[event.target.name + "-group"].classList.add("has-error");
        }
    }

    render() {
        return (
            <div className="login-component">
                <div className="login-component-container">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 col-md-offset-4">
                                <div className="panel panel-default">
                                    <div className="panel-body">
                                        <h3>Login</h3>

                                        <form onSubmit={this.submitHandler.bind(this)}>
                                            <div id="username-group" className="form-group form-group-user" ref={this.refElement.bind(this)}>
                                                <label className="control-label">Usuário</label>
                                                <input
                                                    onBlur={this.onBlurHandler.bind(this)}
                                                    onChange={this.onChangeHandler.bind(this)}
                                                    name="username"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.username} />
                                            </div>

                                            <div id="password-group" className="form-group form-group-password" ref={this.refElement.bind(this)}>
                                                <label className="control-label">Senha</label>
                                                <input
                                                    onBlur={this.onBlurHandler.bind(this)}
                                                    onChange={this.onChangeHandler.bind(this)}
                                                    name="password"
                                                    type="password"
                                                    className="form-control"
                                                    value={this.state.password} />
                                            </div>

                                            <div id="account_name-group" className="form-group form-group-account" ref={this.refElement.bind(this)}>
                                                <label className="control-label">Conta</label>
                                                <input
                                                    onBlur={this.onBlurHandler.bind(this)}
                                                    onChange={this.onChangeHandler.bind(this)}
                                                    name="account_name"
                                                    type="text"
                                                    className="form-control"
                                                    value={this.state.account_name} />
                                            </div>

                                            <div className={this.state.error ? 'alert alert-danger' : 'hidden'}>{this.state.error}</div>

                                            <button className="btn btn-primary">Login</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    refElement(element) {
        if(element) {
            this.elements[element.id] = element;
        }
    }

    validate() {
        var error = false;

        if(!this.state.username) {
            this.elements["username-group"].classList.add("has-error");
            error = true;
        }

        if(!this.state.password) {
            this.elements["password-group"].classList.add("has-error");
            error = true;
        }

        if(!this.state.account_name) {
            this.elements["account_name-group"].classList.add("has-error");
            error = true;
        }

        return !error;
    }

    onAuthSuccess(response) {
        if(response.data.status == "error") {
            switch (response.data.error) {
                case "401":
                case "400":
                    this.state.error = "Credenciais inválidas. Por favor, verifique e tente novamente.";
                    this.setState(this.state);
                    break;
                default:
            }

            return;
        }

        localStorage.setItem("token", response.data.auth_token);
        localStorage.setItem("account_id", response.data.data.account_id);
        localStorage.setItem("account_name", response.data.data.account_name);
        hashHistory.push("/admin");
    }

    onAuthError(error) {
        this.state.error = "Credenciais inválidas. Por favor, verifique e tente novamente.";
        this.setState(this.state);
    }
}

export class AutoLoginComponent extends Component {

    componentWillMount() {
        $("body").addClass("page-auto-login");

        if(!!this.props.routeParams.credentials && !!this.props.routeParams.account_name) {
            auth(this.props.routeParams.credentials, this.props.routeParams.account_name)
                .then(this.onAuthSuccess.bind(this))
                .catch(this.onAuthError.bind(this));
        }
    }

    componentWillUnmount() {
        $("body").removeClass("page-auto-login");
    }

    render() {
        return (
            <div className="auto-login-component">
                <img className="img-loading" src={loading} />
            </div>
        );
    }

    onAuthSuccess(response) {
        if(response.data.status == "error") {
            switch (response.data.error) {
                case "401":
                case "400":
                    this.state.error = "Credenciais inválidas. Por favor, verifique e tente novamente.";
                    this.setState(this.state);
                    break;
                default:
            }

            return;
        }

        localStorage.setItem("token", response.data.auth_token);
        localStorage.setItem("account_id", response.data.data.account_id);
        localStorage.setItem("account_name", response.data.data.account_name);
        hashHistory.push("/admin");

    }

    onAuthError(error) {
        hashHistory.push("/login");
    }

}
