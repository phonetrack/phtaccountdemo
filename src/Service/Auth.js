import axios from "axios";

/**
 * Performs the remote authentication
 * @param String credentials: Credentials md5 hash
 * @param String account_name: Account name
 * @return Promise
 */
export function auth(credentials, account_name) {
    return axios({
        url: `${App.config.apiEndpointPrefix}/v2/user_auth`,
        method: 'put',
        data: {
            data: {
                credentials: credentials,
                account_name: account_name
            }
        },
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    });
}
