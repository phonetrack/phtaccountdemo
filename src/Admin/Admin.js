import React, { Component, cloneChildren } from "react";
import { hashHistory, Link } from 'react-router';
import axios from "axios";

export class AdminComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            account: {
                id: localStorage.getItem("account_id"),
                name: localStorage.getItem("account_name")
            },
            children: []
        }
    }

    componentWillMount() {
        if(!localStorage.getItem("token")) {
            hashHistory.push("/login");
        }

        this.getChildren();
    }

    getChildren() {
        axios({
            url: `${App.config.apiEndpointPrefix}/v2/accounts/${this.state.account.id}/children`,
            method: 'get',
            headers: {
                "X-Auth-Token": localStorage.getItem("token")
            }
        })
        .then(this.onGetChildrenSuccess.bind(this))
        .catch(this.onGetChildrenError.bind(this));
    }

    render() {
        if(!localStorage.getItem("token")) {
            hashHistory.push("/login");
        }

        let children = this.state.children.map((item) => {
            return (
                <li key={item.id}><a onClick={this.onChildClick.bind(this, item)}>{item.name}</a></li>
            );
        });

        return(
            <div>
                <nav className="navbar navbar-default navbar-fixed-top">
                    <div className="container">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <Link className="navbar-brand" to="/admin">HypnoPhone</Link>
                            </div>
                            <ul className="nav navbar-nav pull-right">
                                <li className="dropdown">
                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{this.state.account.name} <span className="caret"></span></a>
                                    <ul className="dropdown-menu">
                                        <li><Link onClick={this.onChildClick.bind(this, {id: localStorage.getItem("account_id"), name: localStorage.getItem("account_name")})}>{localStorage.getItem("account_name")}</Link></li>
                                        {children}
                                    </ul>
                                </li>
                                <li>
                                    <Link onClick={this.onLogoutClick.bind(this)}>
                                        <span className="glyphicon glyphicon-log-out"></span>
                                    </Link>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            {this.props.children && React.cloneElement(this.props.children, {account: this.state.account})}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onChildClick(item) {
        this.state.account.id = item.id;
        this.state.account.name = item.name;
        this.setState(this.state);
    }

    onLogoutClick(event) {
        event.preventDefault();
        localStorage.clear();
        hashHistory.push("/");
    }

    onGetChildrenSuccess(response) {
        if(response.data.status != "error") {
            this.state.children = response.data.data;
            this.setState(this.state);
        }
    }

    onGetChildrenError(error) {

    }

};
