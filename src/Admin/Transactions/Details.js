import React, { Component } from "react";

export class AdminTransactionsDetailsComponent extends Component {
    getRows() {
        return this.props.data.map(function(item, index) {
            var date, from, to;

            if(item.created) {
                date = new Date((item.created - 62167219200) * 1000);
                from = item.metadata.from.split("@");
                to = item.metadata.to.split("@");

                return (
                    <tr key={index} className={item.type}>
                        <td><span className={"glyphicon glyphicon-arrow-" + (item.type == 'credit' ? "right" : "left")}></span></td>
                        <td>{date.getDate()}/{date.getMonth()}/{date.getFullYear()} {date.getHours()}:{date.getMinutes()}:{date.getSeconds()}</td>
                        <td>{from[0]}</td>
                        <td>{to[0]}</td>
                        <td>{item.sub_account_name}</td>
                        <td>{Math.ceil(item.metadata.duration / 60)}</td>
                        <td>R$ {item.amount.toFixed(2).replace(".", ",")}</td>
                    </tr>
                );
            }
        });
    }

    render() {
        return(
            <div className="admin-transactions-details-component">
                <div className="details-table">
                    <table className="table table-stripped table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Data</th>
                                <th>De</th>
                                <th>Para</th>
                                <th>Conta</th>
                                <th>Minutos</th>
                                <th>Crédito consumido</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.getRows()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
