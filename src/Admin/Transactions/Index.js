import React, { Component } from "react";
import { hashHistory } from 'react-router';
import { stringify } from "qs";
import axios from "axios";
import { AdminTransactionsWidgetComponent } from "./Widget";
import { AdminTransactionsFiltersComponent } from "./Filters";
import { AdminTransactionsDetailsComponent } from "./Details";

export class AdminTransactionsIndexComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            amount: 0,
            callCharges: 0,
            minutesUsed: 0,
            data: [],
            account: {
                id: this.props.account.id,
                name: this.props.account.name
            }
        };
    }

    componentWillMount() {
        this.update();
        this.getCurrentBalance();
    }

    render() {
        if(this.state.account.id !== this.props.account.id) {
            this.state.account.id = this.props.account.id;
            this.state.account.name = this.props.account.name;
            this.update();
            this.getCurrentBalance();
        }

        return (
            <div className="admin-transactions-index-component">
                <div className="row">
                    <div className="col-md-12">
                        <AdminTransactionsWidgetComponent amount={this.state.amount} callCharges={this.state.callCharges} minutesUsed={this.state.minutesUsed}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <AdminTransactionsFiltersComponent update={this.update.bind(this)} download={this.download.bind(this)} hasData={this.state.data.length > 0}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <AdminTransactionsDetailsComponent data={this.state.data} update={this.update.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }

    download() {
        var str = '';

        for (var i = 0; i < this.state.data.length; i++) {
            var line = '';
            for (var index in this.state.data[i]) {
                if (line != '')
                    line += ','

                line += this.state.data[i][index];
            }

            str += line + '\r\n';
        }

        var uri = 'data:text/csv;charset=utf-8,' + escape(str);
        var downloadLink = document.createElement("a");
        downloadLink.href = uri;
        downloadLink.download = "data.csv";

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }

    update(filters) {
        filters = filters || {};
        filters.reason = "only_calls";

        axios({
            url: `${App.config.apiEndpointPrefix}/v2/accounts/${this.state.account.id}/transactions`,
            method: 'get',
            params: filters,
            headers: {
                "X-Auth-Token": localStorage.getItem("token")
            }
        }).then(this.onUpdateSuccess.bind(this)).catch(this.onUpdateError);
    }

    getCurrentBalance() {
        axios({
            url: `${App.config.apiEndpointPrefix}/v2/accounts/${this.state.account.id}/transactions/current_balance`,
            method: 'get',
            headers: {
                "X-Auth-Token": localStorage.getItem("token")
            }
        }).then(this.onGetCurrentBalanceSuccess.bind(this));
    }

    onGetCurrentBalanceSuccess(response) {
        if (response.data.status == "error") {
            if (response.data.error == 401) {
                ReactRouter.hashHistory.push("/login");
            }
            return;
        }

        this.state.amount = response.data.data.balance;
        this.setState(this.state);
    }

    onUpdateSuccess(response) {
        var dataObj,
            item;

        dataObj = {};

        if (response.data.data.status == "error") {
            if (data.error == 401) {
                ReactRouter.hashHistory.push("/login");
            }

            return;
        }

        this.state.callCharges = 0;
        this.state.minutesUsed = 0;
        this.state.data = response.data.data;

        for (var i in response.data.data) {
            item = response.data.data[i];

            if (item.type == "debit") {
                if (item.amount !== undefined) {
                    this.state.callCharges += item.amount;
                }

                if (item.metadata.duration !== undefined) {
                    this.state.minutesUsed += Math.ceil(item.metadata.duration / 60);
                }
            }

        }

        this.setState(this.state);
    }
}
