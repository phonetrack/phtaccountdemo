# PhT Account demo

## API

### Autenticação
`PUT` `https://accounts-api.phonetrack.com.br/v2/user_auth`

**Form data**
```json
{
    "data": {
        "credentials": "{CREDENTIALS_HASH}",
        "account_name": "{ACCOUNT_NAME}",
        "method": "{MD5_OR_SHA1}"
    }
}
```

**Descrição dos campos**

Campo | Descrição
----- | ---------
credentials | Hash do usuário e senha: Ex: 38d15c0ff1687b08c703a21c52e816b4. Veja abaixo como gerar o hash.
account_name | Nome da conta do usuário.
method | Tipo do hash. md5 ou sha. Padrão md5.

**Geração do hash**

Calcule o valor em md5 ou sha1 para `username:password`. Ex: 'seuusuario@dominio.com:suasenha'

**Exemplo bash**
```bash
$ PASSWORD=`echo -n username:password | md5sum | awk '{print $1}'`
```

**Retorno**

Sucesso:
```javascript
{
    "auth_token": "{AUTH_TOKEN}",
    "data": {
        "account_id": "{ACCOUNT_ID}",
        "apps": [],
        "is_reseller": true,
        "language": "en-US",
        "owner_id": "{OWNER_ID}",
        "reseller_id": "{RESELLER_ID}"
    }
    "request_id": "{REQUEST_ID}",
    "revision": "{REVISION}",
    "status": "success"
}
```

Erro:
```javascript
{
    "data": {
        "message": "invalid credentials"
    },
    "error": "401",
    "message": "invalid_credentials",
    "status": "error",
    "request_id": "770d908c25d7fb39d7d6b8f9a5d51792",
    "auth_token": ""
}
```

### Saldo disponível
`GET` `https://accounts-api.phonetrack.com.br/v2/accounts/{ACCOUNT_ID}/transactions/current_balance`

**headers**

Campo | Descrição
----- | ----
X-Auth-Token | Token gerado pelo serviço de autenticação.

**Retorno**
```javascript
{
    "auth_token": "{AUTH_TOKEN}",
    "data": {
        "balance": 9.18
     },
    "request_id": "{REQUEST_ID}",
    "status": "success"
}
```

### Transações
`GET` `https://accounts-api.phonetrack.com.br/v2/accounts/{ACCOUNT_ID}/transactions`

**Query string**

Campo | Descrição | Tipo
----- | --------- | ----
created_from | Filtrar a partir da data | Timestamp gregoriano
created_to | Filtrar até a data | Timestamp gregoriano
reason | Tipo da transação. *only_calls* para apenas ligações. Vazio para tudo.  | string

**headers**

Campo | Descrição
----- | ---------
X-Auth-Token | Token gerado pelo serviço de autenticação.

**Retorno**
```javascript
{
    "auth_token": "{AUTH_TOKEN}",
    "data": [
        {
            "description": "monthly rollup",
            "id": "09dd02e20e07dbb65401802ba20cfb32",
            "amount": 10.179999999999999716,
            "reason": "database_rollup",
            "type": "credit",
            "created": 63598331974,
            "version": 2,
            "code": 9999
        },
        {
            "metadata": {
                "auth_account_id": "{AUTH_ACCOUNT_ID}"
            },
            "id": "7dd1c20894587e9cbacb2d7fa2de80ab",
            "amount": 1.0,
            "reason": "admin_discretion",
            "type": "debit",
            "created": 63598591394,
            "version": 2,
            "code": 3005
        }
    ],
    "request_id": "{REQUEST_ID}",
    "status": "success"
}
```

### Contas filhas
`GET` `https://accounts-api.phonetrack.com.br/v2/accounts/{ACCOUNT_ID}/children`

**headers**

Campo | Descrição
----- | ----
X-Auth-Token | Token gerado pelo serviço de autenticação.

**Retorno**
```javascript
{
    "page_size": 1,
    "start_key": "",
    "data": [{
        "id": "18278a218ca4157b3be019d6246e9c5e",
        "name": "Hypnobox",
        "realm": "realm1.sip.phonetrack.com.br",
        "descendants_count": 1
    }],
    "revision": "b6de2680cccc750e8d6d4af45b374b3b",
    "request_id": "d49a2d0f718999e726316e7317813c5a",
    "status": "success",
    "auth_token": "aaa8e1e35b80ada61e51e14eeab6f688"
}
```

## Instalação
```bash
$ npm install
```
## Configurações
As configurações estão localizadas no arquivo `index.html`
```html
<script type="text/javascript">
    window.App = window.App || {};

    App.config = {
        apiEndpointPrefix: "https://accounts-api.phonetrack.com.br"
    };
</script>
```

Parâmetro | Descrição
--------- | ---------
apiEndpointPrefix | Prefixo do endpoint para API.

## Comandos
Para executar os comandos, é necessário ter o expressjs global no ambiente.

`npm start`: Sobe um servidor local na porta *3003*
