var webpack = require("webpack");

module.exports = {
    entry: [
        './src/main.js'
    ],
    output: {
        path: '.',
        filename: 'app.bundle.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel"
        }, {
            test: /\.css$/,
            loader: "style-loader!css-loader"
        }, {
            test: /\.(eot|svg|ttf|woff|woff2|gif)$/,
            loader: "url"
        }, {
            test: /\.svg/,
            loader: 'svg-url-loader'
        }]
    },
    resolve: {
        alias: {
            jquery: "jquery/src/jquery"
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ],
    devServer: {
        inline: true,
        contentBase: './',
        port: 3333
    },
    url: {
        dataUrlLimit: 1024 //1kb
    }
};
